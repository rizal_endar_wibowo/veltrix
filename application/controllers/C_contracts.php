<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_contracts extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_master_quote_tarif', "master_quote_tarif");
    $this->load->model('M_master_quote_tnc', 'master_quote_tnc');
    $this->load->model('M_contracts', 'contracts');
    $this->load->model('M_contracts_details', 'contracts_details');
    $this->load->model('M_contracts_package', 'contracts_package');
    $this->load->model('M_contracts_payment_plants', 'contracts_payment_plants');
    $this->load->model('M_users', 'users');
  }

  public function index()
  {
    $data = [
      'contents' => 'contents/contracts/index',
      'quote_tnc' => $this->master_quote_tnc->get_all(),
      'quote_tarif' => $this->master_quote_tarif->get_all_by_group(),
    ];
    $this->load->view('master', $data);
  }

  public function list()
  {
    echo json_encode($this->contracts->get_list());
  }

  public function detail()
  {
    $id = $this->input->post('id');

    $data = $this->contracts->get_detail($id);

    $this->load->view('contents/contracts/detail', $data);
  }

  public function pdf($id)
  {
    $data = $this->contracts->get_detail($id);
    $filename = "Document-$id.pdf";
    $mpdf = new \Mpdf\Mpdf(['format' => 'A4-L']);
    $html = $this->load->view('contents/contracts/pdf', $data, TRUE);
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename, I);
  }

  public function print($id)
  {
    $data = $this->contracts->get_detail($id);
    $filename = "Document-$id.pdf";
    $mpdf = new \Mpdf\Mpdf(['format' => 'A4-L']);
    $html = $this->load->view('contents/contracts/pdf', $data, TRUE);
    $mpdf->WriteHTML($html);
    $mpdf->SetJS('print();');
    $mpdf->Output($filename, I);
  }

  public function delete()
  {
    $post = $this->input->post();
    $id = $post['id'];
    $id_user1 = $post['id1'];
    $id_user2 = $post['id2'];

    $this->users->delete(['id' => $id_user1]);
    $this->users->delete(['id' => $id_user2]);
    $this->contracts->delete(['id' => $id]);
    $this->contracts_details->delete(['id' => $id]);
    $this->contracts_package->delete(['id' => $id]);

    $response = [
      'success' => true,
      'message' => 'Success!',
    ];

    echo json_encode($response);
  }
}
