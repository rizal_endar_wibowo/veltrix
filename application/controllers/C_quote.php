<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_quote extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_master_quote_tarif', "master_quote_tarif");
    $this->load->model('M_master_quote_tnc', 'master_quote_tnc');
    $this->load->model('M_contracts', 'contracts');
  }

  public function index()
  {
    $data = [
      'contents' => 'contents/quote/index',
      'quote_tnc' => $this->master_quote_tnc->get_all(),
      'quote_tarif' => $this->master_quote_tarif->get_all_by_group(),
    ];
    $this->load->view('master', $data);
  }

  public function save()
  {
    $post = $this->input->post();
    $this->contracts->save($post);
  }
}
