<div class="container-fluid">
  <!-- Contents -->

  <div class="row align-items-center">
    <div class="col-sm-12">
      <div class="float-right d-none d-md-block">

      </div>
    </div>
  </div>

  <br>

  <form id="form-add" class="form-horizontal">

    <div class="row">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <h4 class="card-header mt-0">
                Form Add Clients
              </h4>
              <div class="card-body">

                <div class="form-group row">
                  <label for="name1" class="col-lg-3 col-form-label">Name Client 1</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="name1" name="name[]" placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email1" class="col-lg-3">Email Client 1</label>
                  <div class="col-lg-9">
                    <input type="email" class="form-control" id="email1" name="email[]" placeholder="" required="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="cell_number1" class="col-lg-3">Cell Client 1</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control phone" id="cell_number1" onkeypress="if(this.value.length==14) return false;" name="cell_number[]" placeholder="(xxx) xxx-xxxx" required="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="name2" class="col-lg-3 col-form-label">Name Client 2</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="name2" name="name[]" placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email2" class="col-lg-3">Email Client 2</label>
                  <div class="col-lg-9">
                    <input type="email" class="form-control" id="email2" name="email[]" placeholder="" required="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="cell_number2" class="col-lg-3">Cell Client 2</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control phone" id="cell_number2" onkeypress="if(this.value.length==14) return false;" name="cell_number[]" placeholder="(xxx) xxx-xxxx" required="">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="control-label col-lg-3">Event Date</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="event_date" name="event_date" value="2021-07-31">
                  </div>
                </div>

                <hr>
                <div class="form-group row">
                  <label class="control-label col-lg-3">Subtotal Fee</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="subtotal_" readonly="">
                    <input type="hidden" class="form-control" name="subtotal" id="subtotal" readonly="" value="0">
                    <!-- <input id="fee" class="form-control" onkeypress="return numericOnly(this)" ondrop="return false" onpaste="return false" type="text" name="subtotal_fee" placeholder="Subtotal Fee" readonly> -->
                  </div>
                </div>

                <div class="form-group row">
                  <label class="control-label col-lg-3">Tax (8.25%)</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="tax_" readonly="">
                    <input type="hidden" class="form-control" name="tax" id="tax" readonly="">
                    <!-- <input id="tax" class="form-control" onkeypress="return numericOnly(this)" ondrop="return false" onpaste="return false" type="text" name="tax" placeholder="tax" readonly value="8.25"> -->
                  </div>
                </div>

                <div class="form-group row">
                  <label class="control-label col-lg-3">Grand Total :</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="grandtotal_" readonly="">
                    <input type="hidden" class="form-control" name="grandtotal" id="grandtotal" readonly="">
                    <!-- <input type="number" class="form-control" readonly> -->
                  </div>
                </div>

                <hr>

                <label for="">Details Packages: </label>
                <table class="table nowrap">
                  <thead>
                    <tr>
                      <th style="width: 10%;">ID</th>
                      <th style="width: 30%;">Package</th>
                      <th style="width: 60%;">Details</th>
                    </tr>
                  </thead>
                  <tbody id="table-details">
                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">

              <h4 class="card-header mt-0">
                Payment Plans
              </h4>

              <div class="card-body">

                <div class="alert alert-warning" role="alert" style="display: none;" id="alert-kurang"></div>
                <div class="alert alert-danger" role="alert" style="display: none;" id="alert-lebih"></div>
                <div class="alert alert-success" role="alert" style="display: none;" id="alert-pas"></div>

                <table class="table">
                  <tbody>
                    <tr>
                      <th>No. </th>
                      <th>Amount</th>
                      <th>Due Date</th>
                      <th>Desc</th>
                    </tr>

                    <tr>
                      <td>1.</td>
                      <td>
                        <input type="text" id="paytotal_1" class="form-control paytotal_">
                        <input type="hidden" class="form-control" id="amount_1" name="amount[]">
                      </td>
                      <td>
                        <input type="text" class="form-control due_date" name="due_date[]" id="due_date_1" value="2021-07-31">
                      </td>
                      <td>
                        <textarea class="form-control" name="desc[]" cols="30" rows="1"></textarea>
                      </td>
                    </tr>


                    <tr>
                      <td>2.</td>
                      <td>
                        <input type="text" id="paytotal_2" class="form-control paytotal_">
                        <input type="hidden" class="form-control" id="amount_2" name="amount[]">
                      </td>
                      <td>
                        <input type="text" class="form-control due_date" name="due_date[]" id="due_date_2" value="2021-07-31">
                      </td>
                      <td>
                        <textarea class="form-control" name="desc[]" cols="30" rows="1"></textarea>
                      </td>
                    </tr>


                    <tr>
                      <td>3.</td>
                      <td>
                        <input type="text" id="paytotal_3" class="form-control paytotal_">
                        <input type="hidden" class="form-control" id="amount_3" name="amount[]">
                      </td>
                      <td>
                        <input type="text" class="form-control due_date" name="due_date[]" id="due_date_3" value="2021-07-31">
                      </td>
                      <td>
                        <textarea class="form-control" name="desc[]" cols="30" rows="1"></textarea>
                      </td>
                    </tr>

                    <tr>
                      <td>4.</td>
                      <td>
                        <input type="text" id="paytotal_4" class="form-control paytotal_">
                        <input type="hidden" class="form-control" id="amount_4" name="amount[]">
                      </td>
                      <td>
                        <input type="text" class="form-control due_date" name="due_date[]" id="due_date_4" value="2021-07-31">
                      </td>
                      <td>
                        <textarea class="form-control" name="desc[]" cols="30" rows="1"></textarea>
                      </td>

                    </tr>

                  </tbody>
                </table>

              </div>

            </div>
          </div>

        </div>
      </div>

      <div class="col-lg-6">
        <div class="card">

          <h4 class="card-header mt-0">
            Form Contracts &amp; Terms </h4>

          <div class="card-body">
            <?php foreach ($quote_tnc as $key => $q_tnc) : ?>
              <div class="form-check">
                <input class="form-check-input master_contract_terms" required type="checkbox" value="" id="tnc-check-<?= $key ?>" <?php foreach ($q_tnc as $key1 => $value) {
                                                                                                                                      echo 'data-' . $key1 . '="' . $value . '"';
                                                                                                                                    } ?>>
                <input type="hidden" name="id_master_contract_terms[]">
                <input type="hidden" name="title_master_contract_terms[]">
                <input type="hidden" name="contract_terms[]">
                <label class="form-check-label" for="tnc-check-<?= $key ?>">
                  <strong><?= $q_tnc['title']  ?></strong>
                </label>
              </div>
            <?php endforeach ?>

          </div>

        </div>
        <div class="card">

          <h4 class="card-header mt-0">
            Form Package List
          </h4>

          <div class="card-body">

            <?php $i = 0;
            foreach ($quote_tarif as $key => $q_tarif) : ?>
              <div class="row mb-3">
                <label class="control-label col-sm-12 "><?= $key ?> :</label>
                <div class="col-sm-12">
                  <?php foreach ($q_tarif as $key1 => $tarif) : ?>
                    <div class="form-check">
                      <input class="form-check-input tarif" type="checkbox" value="" id="tarif-check-<?= "$i-$key1" ?>" <?php foreach ($tarif as $key2 => $value) {
                                                                                                                          echo 'data-' . (($key2 == 'id' or $key2 == 'harga') ? $key2 . '_tarif' : $key2) . '="' . $value . '"';
                                                                                                                        } ?>>
                      <input type="hidden" name="id_tarif[]">
                      <input type="hidden" name="nama_tarif[]">
                      <input type="hidden" name="harga_tarif[]">
                      <input type="hidden" name="isi_paket[]">
                      <input type="hidden" name="group[]">
                      <label class="form-check-label" for="tarif-check-<?= "$i-$key1" ?>">
                        <strong><?= $tarif['nama_tarif'] . ' - ' . dollar($tarif['harga']) ?></strong>
                      </label>
                    </div>
                  <?php endforeach ?>
                </div>
              </div>

            <?php $i++;
            endforeach ?>

          </div>

        </div>
        <div class="panel-footer">
          <div class="form-group">
            <div class="col-lg-offset-9 col-lg-12">
              <a href="<?= base_url("contracts") ?>" class="btn btn-secondary back"> Cancel</a>
              <button type="submit" class="btn btn-primary btn-order">Order</button>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>


</form>

<input type="hidden" name="title_master_contract_terms[]">

<script>
  $(document).ready(function(e) {
    $('#form-add').submit(function(e) {
      e.preventDefault();
      $('.phone').each(function(index, item) {
        number = $(this).val().replace(/[^\d]/g, '');
        $(this).val(number);
      });
      data = $(this).serialize();
      $('.btn-order').prop('disabled', true);
      $('.btn-order').html("Loading...");
      $.ajax({
        url: '<?= base_url('quote/save') ?>',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: (response) => {
          $('.btn-order').prop('disabled', false);
          $('.btn-order').html("Order");

          if (response.success) {
            $('#form-add')[0].reset();
            alert(response.message)
            window.location("<?= base_url('contracts') ?>")
            return false;
          } else {
            alert(response.message)
            return false;
          }

        },
        error: (e) => {
          alert(`${e.status} - ${e.statusText}`);
          $('.btn-order').prop('disabled', false);
          $('.btn-order').html("Order");

          console.log(e);
        }
      });

    });

    $('.phone').on('input', function() {
      var number = $(this).val().replace(/[^\d]/g, '')
      if (number.length == 7) {
        number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
      } else if (number.length == 10) {
        number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
      }
      $(this).val(number)
    });

    subtotal = 0;
    grandtotal = 0;
    $('#subtotal').val(subtotal);


    $('.master_contract_terms').click(function(e) {

      id = $(this).data('id');
      title = $(this).data('title');
      contract_terms = $(this).data('contract_terms');


      if ($(this).is(':checked') == true) {
        $(this).val(contract_terms);
        $(this).parent().find('input[name^="id_master_contract_terms"]').val(id);
        $(this).parent().find('input[name^="title_master_contract_terms"]').val(title);
        $(this).parent().find('input[name^="contract_terms"]').val(contract_terms);

      } else {
        $(this).val('');
        $(this).parent().find('input[name^="id_master_contract_terms"]').val('');
        $(this).parent().find('input[name^="title_master_contract_terms"]').val('');
        $(this).parent().find('input[name^="contract_terms"]').val('');
      }

    })

    $('.tarif').click(function(e) {

      id = $(this).data('id_tarif');
      nama = $(this).data('nama_tarif');
      harga = $(this).data('harga_tarif');
      isi_paket = $(this).data('isi_paket');
      group = $(this).data('group');
      isi_paket_ = isi_paket.split(',')

      if ($(this).is(':checked') == true) {
        $(this).val(harga);
        $(this).parent().find('input[name^="id_tarif"]').val(id);
        $(this).parent().find('input[name^="nama_tarif"]').val(nama);
        $(this).parent().find('input[name^="harga_tarif"]').val(harga);
        $(this).parent().find('input[name^="isi_paket"]').val(isi_paket);
        $(this).parent().find('input[name^="group"]').val(group);
        contents = `<ul>`;
        for (i = 0; i < isi_paket_.length; i++) {
          contents += `<li>` + isi_paket_[i] + `</li>`
        }
        contents += `</ul>`


        var row_table = `<tr id="id-tarif-` + id + `">
                        <td>` + id + `</td>
                        <td>` + nama + `</td>
                        <td>` + contents + `</td>
                      </tr>`;
        $('#table-details').append(row_table);

      } else {
        $('#id-tarif-' + id).remove();
        $(this).val('');
        $(this).parent().find('input[name^="id_tarif"]').val('');
        $(this).parent().find('input[name^="nama_tarif"]').val('');
        $(this).parent().find('input[name^="harga_tarif"]').val('');
        $(this).parent().find('input[name^="isi_paket"]').val('');
      }

      hitung();
      bagi_paytotal();

      $('#alert-pas').text('Jumlah pembayaran pas').show();
    })

    $('#grandtotal_').autoNumeric('init', {
      aSep: ',',
      aDec: '.',
      aSign: '$ '
    });

    $('#subtotal_').autoNumeric('init', {
      aSep: ',',
      aDec: '.',
      aSign: '$ '
    });

    $('.paytotal_').autoNumeric('init', {
      aSep: ',',
      aDec: '.',
      aSign: '$ '
    });

    $('#tax_').autoNumeric('init', {
      aSep: ',',
      aDec: '.',
      aSign: '$ '
    });

    $('#event_date').datetimepicker({
      format: "YYYY-MM-DD",
      showTodayButton: true,
      timeZone: '',
      dayViewHeaderFormat: 'MMMM YYYY',
      stepping: 5,
      locale: moment.locale(),
      collapse: true,
      icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-crosshairs',
        clear: 'fa fa-trash-o',
        close: 'fa fa-times'
      },
      sideBySide: true,
      calendarWeeks: false,
      viewMode: 'days',
      viewDate: false,
      toolbarPlacement: 'bottom',
      widgetPositioning: {
        horizontal: 'left',
        vertical: 'bottom'
      }
    });

    var date = new Date().getTime()

    for ($i = 0; $i < 10; $i++) {
      if ($i === 1) {
        tanggal = new Date(date + 1209600000);
      } else if ($i === 2) {
        var ambil_tanggal_sebelumnya = $('#due_date_1').data('DateTimePicker').date()
        var convert_to_interger = new Date(ambil_tanggal_sebelumnya).getTime();
        var tanggal = new Date(convert_to_interger + 7776000000)
      } else if ($i === 3) {
        var ambil_tanggal_sebelumnya = $('#due_date_2').data('DateTimePicker').date()
        var convert_to_interger = new Date(ambil_tanggal_sebelumnya).getTime();
        var tanggal = new Date(convert_to_interger + 7776000000)
      } else if ($i === 4) {
        var ambil_tanggal_sebelumnya = $('#due_date_3').data('DateTimePicker').date()
        var convert_to_interger = new Date(ambil_tanggal_sebelumnya).getTime();
        var tanggal = new Date(convert_to_interger + 7776000000)
      }
      $('#due_date_' + $i).datetimepicker({
        format: "YYYY-MM-DD",
        date: tanggal,
        showTodayButton: true,
        timeZone: '',
        dayViewHeaderFormat: 'MMMM YYYY',
        stepping: 5,
        locale: moment.locale(),
        collapse: true,
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-crosshairs',
          clear: 'fa fa-trash-o',
          close: 'fa fa-times'
        },
        sideBySide: true,
        calendarWeeks: false,
        viewMode: 'days',
        viewDate: false,
        toolbarPlacement: 'bottom',
        widgetPositioning: {
          horizontal: 'left',
          vertical: 'bottom'
        }
      });
    }

    function hitung() {
      subtotal = 0;
      $('#subtotal_').autoNumeric('set', 0);
      $('#subtotal').val(0);
      $('#grandtotal_').autoNumeric('set', 0);
      $('#grandtotal').val(0);
      $('#tax_').autoNumeric('set', 0);
      $('#tax').val(0);
      $('.tarif').each(function(index, item) {
        harga_tarif = $(item).val();
        if (harga_tarif != '') {
          subtotal += parseFloat(harga_tarif);
          tax = 0.0825 * parseFloat(subtotal)
          grandtotal = parseFloat(subtotal) + parseFloat(tax)
          $('#subtotal_').autoNumeric('set', subtotal);
          $('#subtotal').val(subtotal);
          $('#tax_').autoNumeric('set', tax);
          $('#tax').val(tax);
          $('#grandtotal_').autoNumeric('set', grandtotal);
          $('#grandtotal').val(grandtotal);
        }
      });
    }

    function bagi_paytotal() {
      grandtotal = $('#grandtotal_').autoNumeric('get');
      if (grandtotal != '') {
        grandtotal_bagi = grandtotal / 4;
        grandtotal_bagi.toFixed(2)
        for (var i = 1; i <= 4; ++i) {
          $('#paytotal_' + i).autoNumeric('set', grandtotal_bagi);
          $('#paytotal_hidden_' + i).val(grandtotal_bagi);
          $('#amount_' + i).val(grandtotal_bagi);
        }
      }
    }
    $('.paytotal_').change(function(e) {
      e.preventDefault();
      grandtotal = $('#grandtotal_').autoNumeric('get');
      grandtotal = parseFloat(grandtotal).toFixed(2)
      var grandtotal_paytotal = 0;
      for (var i = 1; i <= $('.paytotal_').length; ++i) {
        input_paytotal = $('#paytotal_' + i).autoNumeric('get') || 0;
        $('#amount_' + i).val(parseFloat(input_paytotal));
        grandtotal_paytotal += parseFloat(input_paytotal)
      }
      grandtotal_paytotal.toFixed(2)
      if (grandtotal > grandtotal_paytotal) {
        $('#alert-pas').hide();
        $('#alert-lebih').hide();
        $('#alert-kurang').text('Maaf, jumlah pembayaran kekurang : ' + (grandtotal - grandtotal_paytotal).toFixed(2)).show();
      } else if (grandtotal_paytotal > grandtotal) {
        $('#alert-kurang').hide();
        $('#alert-pas').hide();
        $('#alert-lebih').text('Maaf, jumlah pembayaran kelebihan : ' + (grandtotal_paytotal - grandtotal).toFixed(2)).show();
      } else {
        $('#alert-lebih').hide();
        $('#alert-kurang').hide();
        $('#alert-pas').text('Jumlah pembayaran pas').show();
      }
    });

    function total_pas() {
      $('#alert').text('Total pembayaran cukup');
    }

  })
</script>

</div>