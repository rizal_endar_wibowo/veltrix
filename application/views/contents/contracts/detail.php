<?php
function dollar($amount)
{
  $formatter = new NumberFormatter('en-US',  NumberFormatter::CURRENCY);
  return $formatter->formatCurrency(floatval($amount), 'USD');
}
?>
<!-- start page title -->
<div class="row align-items-center">
  <div class="col-sm-6">
    <div class="page-title-box">
      <!-- <h4 class="font-size-18">Contract List</h4> -->
    </div>
  </div>

  <div class="col-sm-6">
    <div class="float-right d-none d-md-block">
      <a href="<?= base_url('contracts/print/' . $data['id']) ?>" class="btn btn-success waves-effect waves-light print">Print</a>
      <a href="<?= base_url('contracts') ?>" class="btn btn-primary waves-effect waves-light">Back</a>
    </div>
  </div>
</div>
<!-- end page title -->

<div class="row">
  <div class="col-lg-6">
    <div class="card">
      <h4 class="card-header mt-0">
        Data Contracts
      </h4>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-5">
            <span class="font-weight-bold">Event Date</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['event_date'] ?></span>
          </div>
          <div class="col-sm-5">
            <span class="font-weight-bold">Contract Number</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['contract_number'] ?></span>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-5">
            <span class="font-weight-bold">Full Name</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['nama1'] ?></span>
          </div>
          <div class="col-sm-5">
            <span class="font-weight-bold">Cell Number</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['cell_number1'] ?></span>
          </div>
          <div class="col-sm-5">
            <span class="font-weight-bold">Email Address</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['email1'] ?></span>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-sm-5">
            <span class="font-weight-bold">Full Name</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['nama2'] ?></span>
          </div>
          <div class="col-sm-5">
            <span class="font-weight-bold">Cell Number</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['cell_number2'] ?></span>
          </div>
          <div class="col-sm-5">
            <span class="font-weight-bold">Email Address</span>
          </div>
          <div class="col-sm-7">
            <span>: <?= $data['email2'] ?></span>
          </div>
        </div>
        <hr>
        <div class="row">
          <?php foreach ($payment as $key => $pay) : ?>
            <div class="col-sm-3">
              <span class="font-weight-bold"><?= $pay['payment_sequence'] ?></span>
            </div>
            <div class="col-sm-4">
              <span>: <?= dollar($pay['amount']) ?></span>
            </div>
            <div class="col-sm-2">
              <span class="font-weight-bold">Due</span>
            </div>
            <div class="col-sm-3">
              <span>: <?= $pay['due_date'] ?></span>
            </div>
          <?php endforeach ?>
        </div>
        <hr>
        <div class="row">
          <div class="col-4">
            <p>Client 1</p>
          </div>
          <div class="col-4">
            <p>Client 2</p>
          </div>
          <div class="col-4">
            <p>Signature 3</p>
          </div>
        </div>

      </div>
    </div>

  </div>
  <div class="col-lg-6">
    <div class="card">
      <h4 class="card-header mt-0">
        Details Contracts
      </h4>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <?php foreach ($details as $key => $detail) : ?>
              <span class="font-weight-bold"><?= $detail['nama_tarif'] . " - " . dollar($detail['harga']) ?></span>
              <ul>
                <?php
                if ($detail['isi_paket'] != null) :
                  $isi_paket = explode(",", $detail['isi_paket']);
                  foreach ($isi_paket as $paket) : ?>
                    <li><?= $paket ?></li>
                <?php endforeach;
                endif ?>
              </ul>
            <?php endforeach ?>

            <div class="row">
              <div class="col-sm-5">
                <span>Sub Total</span>
              </div>
              <div class="col-sm-7">
                <span>: <?= $subtotal ?></span>
              </div>
              <div class="col-sm-5">
                <span>Tax</span>
              </div>
              <div class="col-sm-7">
                <span>: <?= $tax ?></span>
              </div>
              <div class="col-sm-5">
                <span>Grand Total</span>
              </div>
              <div class="col-sm-7">
                <span>: <?= $grand_total ?></span>
              </div>
              <div class="col-sm-5">
                <span>Total payment</span>
              </div>
              <div class="col-sm-7">
                <span>: <?= $total_payment ?></span>
              </div>
              <div class="col-sm-5">
                <span>Balance</span>
              </div>
              <div class="col-sm-7">
                <span>: <?= $balance ?></span>
              </div>
            </div>

            <table class="table nowrap">
              <thead>
                <tr>
                  <th></th>
                  <th>Tanggal</th>
                  <th>Kode Transaksi</th>
                  <th>No COA</th>
                  <th>Jumlah</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <h4 class="card-header mt-0">
        Contracts Details & Terms
      </h4>
      <div class="card-body">
        <?php
        $contract_terms = json_decode($data['contract_terms']);
        foreach ($contract_terms as $ct) {
          echo "<h5 class=''>{$ct->title}</h5>";
          echo $ct->contract_terms . '<hr>';
        } ?>
      </div>
    </div>

  </div>

</div>