<div class="container-fluid main">

  <!-- start page title -->
  <div class="row align-items-center">
    <div class="col-sm-6">
      <div class="page-title-box">
        <h4 class="font-size-18">Contract List</h4>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="float-right d-none d-md-block">
        <a href="<?= base_url('quote') ?>" class="btn btn-primary waves-effect waves-light">Add Contract</a>
      </div>
    </div>
  </div>
  <!-- end page title -->

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">

          <table id="contract-list-table" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead>
              <tr>
                <th>No</th>
                <th></th>
                <th>Event Date</th>
                <th>Client Names</th>
                <th>Contract Number</th>
                <th>Nama-Email-Cell Number-Client 1</th>
                <th>Nama-Email-Cell Number-Client 2</th>
                <th>Subtotal Fee</th>
                <th>Tax</th>
                <th>Grand Total</th>
              </tr>
            </thead>


            <tbody></tbody>
          </table>

        </div>
      </div>
    </div> <!-- end col -->
  </div> <!-- end row -->

  <script>
    $(document).ready(function() {
      $("#contract-list-table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": '<?= base_url('contracts/list') ?>',
          "type": "POST",
          "dataType": "json",
          async: "true"
        },
      })

      $(document).on('click', '.detail', function(e) {
        e.preventDefault();
        console.log('id');
        $.ajax({
          url: "<?= base_url('contracts/detail') ?>",
          type: "POST",
          data: {
            id: $(this).data('id')
          },
          success: (html) => {
            $(".main").html(html);
          },
          error: (e) => {
            alert(`${e.status} - ${e.statusText}`);
          }
        });
      })

      $(document).on('click', '.delete', function(e) {
        e.preventDefault();
        var confirm = window.confirm("Yakin hapus data ini ?");
        if (confirm == true) {
          $.ajax({
            url: "<?= base_url('contracts/delete') ?>",
            type: "POST",
            data: {
              id: $(this).data('id'),
              id1: $(this).data('user1'),
              id2: $(this).data('user2')
            },
            dataType: 'json',
            success: (data) => {
              alert(data.message);
              if (data.success) {
                location.reload();
              } else {
                return false;
              }
            },
            error: (e) => {
              alert(`${e.status} - ${e.statusText}`);
            }
          });
        } else {
          return false;
        }
      });

    });
  </script>

</div> <!-- container-fluid -->