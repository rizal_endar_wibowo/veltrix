<?php
function dollar($amount)
{
  $formatter = new NumberFormatter('en-US',  NumberFormatter::CURRENCY);
  return $formatter->formatCurrency(floatval($amount), 'USD');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>


  <table style="width: 100%;">
    <tbody>
      <tr>
        <td style="width: 50.0000%;"><strong><span style="font-family: Arial, Helvetica, sans-serif;">DATA CONTRACTS</span></strong><br><br>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Event Date</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['event_date'] ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Contract Number</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['contract_number'] ?></span></td>
              </tr>
            </tbody>
          </table>
          <hr>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Full Name</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['nama1'] ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Cell Number</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['cell_number1'] ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Email Address</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['email1'] ?></span></td>
              </tr>
            </tbody>
          </table><br>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Full Name</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['nama2'] ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Cell Number</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['cell_number2'] ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Email Address</span></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $data['email2'] ?></span></td>
              </tr>
            </tbody>
          </table>
          <hr>
          <table style="width: 100%;">
            <tbody>
              <?php foreach ($payment as $key => $pay) : ?>

                <tr>
                  <td style="width: 25.0000%;"><span style="font-family: Arial, Helvetica, sans-serif;"><?= $pay['payment_sequence'] ?></span></td>
                  <td style="width: 25.0000%;"><span style="font-family: Courier New, courier;">: <?= dollar($pay['amount']) ?></span></td>
                  <td style="width: 25.0000%;"><span style="font-family: Arial, Helvetica, sans-serif;">Due</span></td>
                  <td style="width: 25.0000%;"><span style="font-family: Courier New, courier;">: <?= $pay['due_date'] ?></span></td>
                </tr>
              <?php endforeach ?>

            </tbody>
          </table>
          <hr>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td style="width: 33.3333%;">
                  <div data-empty="true" style="text-align: center;"><span style="font-family: Arial, Helvetica, sans-serif;">Client 1</span></div>
                </td>
                <td style="width: 33.3333%;">
                  <div data-empty="true" style="text-align: center;"><span style="font-family: Arial, Helvetica, sans-serif;">Client 2</span></div>
                </td>
                <td style="width: 33.3333%;">
                  <div data-empty="true" style="text-align: center;"><span style="font-family: Arial, Helvetica, sans-serif;">Signature 3</span></div>
                </td>
              </tr>
            </tbody>
          </table><br>
        </td>
        <td style="width: 50.0000%; vertical-align: top;"><strong><span style="font-family: Arial, Helvetica, sans-serif;">DETAILS CONTRACTS</span></strong><br><br>
          <?php
          foreach ($details as $key => $detail) : ?>
            <span style="font-family: Arial, Helvetica, sans-serif;"><?= $detail['nama_tarif'] . " - " . dollar($detail['harga']) ?></span>
            <ul>
              <?php
              if ($detail['isi_paket'] != null) :
                $isi_paket = explode(",", $detail['isi_paket']);
                foreach ($isi_paket as $paket) : ?>
                  <li style="font-family: Arial, Helvetica, sans-serif;"><?= $paket ?></li>
              <?php endforeach;
              endif ?>
            </ul>
          <?php endforeach ?>

          <table style="width: 100%;">
            <tbody>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Sub Total</span><br></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $subtotal ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Tax</span><br></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $tax ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Grand Total</span><br></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $grand_total ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Total Payment</span><br></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $total_payment ?></span></td>
              </tr>
              <tr>
                <td style="width: 40.5498%;"><span style="font-family: Arial, Helvetica, sans-serif;">Balance</span><br></td>
                <td style="width: 59.1066%;"><span style="font-family: Courier New, courier;">: <?= $balance ?></span></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <hr>
  <p><span style="font-family: Arial, Helvetica, sans-serif;"><strong>CONTRACTS DETAILS &amp; TERMS</strong></span></p>
  <?php
  $contract_terms = json_decode($data['contract_terms']);
  foreach ($contract_terms as $ct) {
    echo "<p style='font-family: Courier New, courier;'>{$ct->title}</p>";
    echo '<span style="font-family: Courier New, courier;">' . $ct->contract_terms . '</span><hr>';
  } ?>

</body>

<script>
  <?php if (isset($print)) : ?>
    window.print();
  <?php endif ?>
</script>

</html>