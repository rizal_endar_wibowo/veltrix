<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <a class="waves-effect" href="<?= base_url('contracts') ?>">
                        <span>Contracts</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect" href="<?= base_url('quote') ?>">
                        <span>Quote</span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->