<?php
class M_contracts_details extends CI_Model
{

  public $table = 'cp_contracts_details';

  public function add_multiple($contracts, $params)
  {
    foreach ($params['id_tarif'] as $key => $id_tarif) {
      if ($id_tarif != '') {
        $add_data_contracts_details[] = [
          'id' => '',
          'id_contracts' => $contracts['id'],
          'id_master_tarif' => $id_tarif,
          'nama_tarif' => $params['nama_tarif'][$key],
          'isi_paket' => $params['isi_paket'][$key],
          'harga' => $params['harga_tarif'][$key],
          'group' => $params['group'][$key],
        ];
      }
    }

    return isset($add_data_contracts_details) ? $this->db->insert_batch($this->table, $add_data_contracts_details) : false;
  }

  public function delete($where)
  {
    return $this->db->delete($this->table, $where);
  }
}
