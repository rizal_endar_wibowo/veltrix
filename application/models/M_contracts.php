<?php
class M_contracts extends CI_Model
{

  protected $table = 'cp_contracts';

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_master_quote_tarif', "master_quote_tarif");
    $this->load->model('M_master_quote_tnc', 'master_quote_tnc');
    $this->load->model('M_contracts_details', 'contracts_details');
    $this->load->model('M_contracts_package', 'contracts_package');
    $this->load->model('M_contracts_payment_plants', 'contracts_payment_plants');
    $this->load->model('M_users', 'users');
  }

  public function save($post)
  {
    $users = $this->users->add_multiple($post);

    if ($users['success']) {
      $id_users = $users['id_users'];
      $contracts = $this->add($users['id_users'], $post);
      if ($contracts['success']) {
        $id_contracts = $contracts['id'];
        $contracts_details = $this->contracts_details->add_multiple($contracts, $post);
        if ($contracts_details) {
          $contracts_package = $this->contracts_package->add_multiple($contracts, $post);
          if ($contracts_package) {
            $contracts_payment_plants = $this->contracts_payment_plants->add_multiple($contracts, $post);
            if ($contracts_payment_plants) {
              $response = [
                'success' => true,
                'message' => 'Success!',
              ];
              echo json_encode($response);
              exit;
            } else {
              foreach ($id_users as $id_user) {
                $this->users->delete(['id' => $id_user]);
              }
              $this->contracts->delete(['id' => $id_contracts]);
              $this->contracts_details->delete(['id' => $id_contracts]);
              $this->contracts_package->delete(['id' => $id_contracts]);

              $response = [
                'success' => false,
                'message' => 'Failed, Try again!',
              ];
              echo json_encode($response);
              exit;
            }
          } else {
            foreach ($id_users as $id_user) {
              $this->users->delete(['id' => $id_user]);
            }
            $this->contracts->delete(['id' => $id_contracts]);
            $this->contracts_details->delete(['id' => $id_contracts]);

            $response = [
              'success' => false,
              'message' => 'Failed, Try again!',
            ];
            echo json_encode($response);
            exit;
          }
        } else {
          foreach ($id_users as $id_user) {
            $this->users->delete(['id' => $id_user]);
          }
          $this->contracts->delete(['id' => $id_contracts]);

          $response = [
            'success' => false,
            'message' => 'Failed, Try again!',
          ];
          echo json_encode($response);
          exit;
        }
      } else {
        foreach ($id_users as $id_user) {
          $this->users->delete(['id' => $id_user]);
        }
      }
    } else {
      echo json_encode($users);
      exit;
    }

    foreach ($id_users as $id_user) {
      $this->users->delete(['id' => $id_user]);
    }
    if (isset($id_contracts)) {
      $this->contracts->delete(['id' => $id_contracts]);
      $this->contracts_details->delete(['id' => $id_contracts]);
      $this->contracts_package->delete(['id' => $id_contracts]);
    }

    $response = [
      'success' => false,
      'message' => 'Failed, Try again!',
    ];
    echo json_encode($response);
  }

  public function add($id_users, $params)
  {
    $response = [];
    foreach ($params['contract_terms'] as $key => $ct) {
      $contract_terms[] = [
        "title" => $params['title_master_contract_terms'][$key],
        "contract_terms" => $ct
      ];
    }
    $contract_terms = json_encode($contract_terms);
    $last_id_contracts = $this->get_last_id();
    $contract_number = "C/" . date('y') . "/" . ($last_id_contracts + 1);

    $add_data_contracts = [
      'id' => '',
      'contract_number' => $contract_number,
      'event_date' => $params['event_date'],
      'id_users1' => $id_users[0],
      'id_users2' => $id_users[1],
      'nama1' => $params['name'][0],
      'nama2' => $params['name'][1],
      'contract_terms' => $contract_terms
    ];

    $this->db->insert($this->table, $add_data_contracts);
    $id = $this->db->insert_id();
    if ($id > 0) {
      $response = [
        'success' => true,
        'id' => $id,
        'contract_number' => $contract_number
      ];
    } else {
      $response = [
        'success' => false,
        'message' => 'Failed, Try Again!',
      ];
    }

    return $response;
  }

  public function get_last_id()
  {
    $row = $this->db->get($this->table)->last_row();
    if ($row) {
      return $row->id;
    }
    return 0;
  }

  public function delete($where)
  {
    return $this->db->delete($this->table, $where);
  }

  public function get_list()
  {
    $data['data'] = array();
    $this->db->select("*, (select cell_number from cp_users where cp_users.id = $this->table.id_users1) as cell_number1, 
    (select cell_number from cp_users where cp_users.id = $this->table.id_users2) as cell_number2,
    (select email from cp_users where cp_users.id = $this->table.id_users1) as email1,
    (select email from cp_users where cp_users.id = $this->table.id_users2) as email2");
    $this->db->from($this->table);
    $contracts = $this->db->get()->result_array();
    if (count($contracts) > 0) {
      foreach ($contracts as $key => $contract) {
        $subtotal = $this->db->select_sum('harga')->where('id_contracts', $contract['id'])->get('cp_contracts_details')->result_array();
        $subtotal = $subtotal[0]['harga'];
        $tax = 0.0825 * $subtotal;
        $grand_total = $subtotal + $tax;

        $opt = '<a href="' . base_url('contracts/pdf/' . $contract['id']) . '" target="_blank" class="btn btn-sm btn-success">PDF</a>';
        $opt .= '<button class="btn btn-sm btn-danger mx-1 delete" data-id="' . $contract['id'] . '" data-user1="' . $contract['id_users1'] . '" data-user2="' . $contract['id_users2'] . '">Delete</button>';
        $opt .= '<button class="btn btn-sm btn-primary detail" data-id="' . $contract['id'] . '">Detail</button>';

        $data['data'][$key] = [
          $key + 1,
          $opt,
          $contract['event_date'],
          $contract['nama1'] . ' & ' . $contract['nama2'],
          $contract['contract_number'],
          $contract['nama1'] . '-' . $contract['email1'] . '-' . $contract['cell_number1'],
          $contract['nama2'] . '-' . $contract['email2'] . '-' . $contract['cell_number2'],
          $this->dollar($subtotal),
          $this->dollar($tax),
          $this->dollar($grand_total)
        ];
      }
    }
    return $data;
  }

  public function get_detail($id)
  {
    $this->db->select("*, (select cell_number from cp_users where cp_users.id = $this->table.id_users1) as cell_number1, 
    (select cell_number from cp_users where cp_users.id = $this->table.id_users2) as cell_number2,
    (select email from cp_users where cp_users.id = $this->table.id_users1) as email1,
    (select email from cp_users where cp_users.id = $this->table.id_users2) as email2");
    $this->db->from($this->table)->where('id', $id);
    $contracts['data'] = $this->db->get()->result_array()[0];
    $contracts['details'] = $this->db->where('id_contracts', $id)->get("cp_contracts_details")->result_array();
    $subtotal = $this->db->select_sum('harga')->where('id_contracts', $id)->get('cp_contracts_details')->result_array();
    $subtotal = $subtotal[0]['harga'];
    $tax = 0.0825 * $subtotal;
    $grand_total = $subtotal + $tax;
    $total_payment = 0;
    $contracts['subtotal'] = $this->dollar($subtotal);
    $contracts['tax'] = $this->dollar($tax);
    $contracts['grand_total'] = $this->dollar($grand_total);
    $contracts['payment'] = $this->db->where('id_contracts', $id)->get("cp_contracts_payment_plans")->result_array();
    $contracts['total_payment'] = $this->dollar($total_payment);
    $contracts['balance'] = $this->dollar($grand_total - $total_payment);

    return $contracts;
  }

  public function dollar($amount)
  {
    $formatter = new NumberFormatter('en-US',  NumberFormatter::CURRENCY);
    return $formatter->formatCurrency($amount, 'USD');
  }
}
