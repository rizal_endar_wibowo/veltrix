<?php
class M_master_quote_tarif extends CI_Model
{

  public $table = 'cp_master_quote_tarif';

  public function get_all()
  {
    return $this->db->get($this->table)->result_object();
  }

  public function get_all_by_group()
  {
    $groups = $this->db->select('group')->group_by('group')->get($this->table)->result_array();

    foreach ($groups as $group) {
      $data[$group['group']] = $this->db->get_where($this->table, ["group" => $group['group']])->result_array();
    }
    return $data;
  }
}
