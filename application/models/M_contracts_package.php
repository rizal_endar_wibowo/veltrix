<?php
class M_contracts_package extends CI_Model
{

  public $table = 'cp_contracts_package';

  public function add_multiple($contracts, $params)
  {

    foreach ($params['isi_paket'] as $key => $isi_paket) {
      $arr_isi_paket = explode(",", $isi_paket);
      foreach ($arr_isi_paket as $value) {
        if ($value != '') {
          $add_data_contracts_package[] = [
            'id' => '',
            'id_contract' => $contracts['id'],
            'nama_tarif' => $params['nama_tarif'][$key],
            'isi_paket_to_do' => $value
          ];
        }
      }
    }
    return isset($add_data_contracts_package) ? $this->db->insert_batch($this->table, $add_data_contracts_package) : false;
  }

  public function delete($where)
  {
    return $this->db->delete($this->table, $where);
  }
}
