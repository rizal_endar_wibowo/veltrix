<?php
class M_users extends CI_Model
{

  public $table = 'cp_users';

  public function add_multiple($params)
  {
    $response = [];
    $id_users = null;
    foreach ($params['name'] as $key => $name) {
      $email = $params['email'][$key];
      $cell_number = $params['cell_number'][$key];

      if ($this->check_email_cell_number($email, $cell_number) > 0) {
        $response = [
          'success' => false,
          'message' => 'Email or Cell Number Client ' . ($key + 1) . ' already used',
        ];
        return $response;
      }
      $add_data_users = [
        'id' => '',
        'name' => $name,
        'email' => $email,
        'cell_number' => $cell_number,
      ];
      $this->db->insert($this->table, $add_data_users);
      $id = $this->db->insert_id();

      $id_users[] = $id > 0 ? $id : null;
    }
    if ($id_users != null) {
      $response = [
        'success' => true,
        'id_users' => $id_users,
      ];
    }

    return $response;
  }

  public function check_email_cell_number($email, $cell_number)
  {
    return $this->db->where(['email' => $email])->or_where(['cell_number' => $cell_number])->get($this->table)->num_rows();
  }

  public function delete($where)
  {
    return $this->db->delete($this->table, $where);
  }
}
