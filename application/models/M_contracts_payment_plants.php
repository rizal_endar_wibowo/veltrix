<?php
class M_contracts_payment_plants extends CI_Model
{

  public $table = 'cp_contracts_payment_plans';

  public function add_multiple($contracts, $params)
  {
    $paymemnt_sequence = $this->payment_sequence(count($params['amount']));
    foreach ($params['amount'] as $key => $amount) {
      $add_data_contracts_payment_plans[] = [
        'id' => '',
        'id_contracts' => $contracts['id'],
        'contract_number' => $contracts['contract_number'],
        'amount' => $amount,
        'due_date' => $params['due_date'][$key],
        'payment_sequence' => $paymemnt_sequence[$key],
        'desc' => $params['desc'][$key]
      ];
    }

    return isset($add_data_contracts_payment_plans) ? $this->db->insert_batch($this->table, $add_data_contracts_payment_plans) : false;
  }

  public function delete($where)
  {
    return $this->db->delete($this->table, $where);
  }

  public function payment_sequence($number)
  {
    for ($i = 1; $i <= $number; $i++) {
      $ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
      if ((($i % 100) >= 11) && (($i % 100) <= 13)) {
        $sequence[] = strtoupper($i . 'th payment');
      } else {
        $sequence[] = strtoupper($i . $ends[$i % 10] . ' payment');
      }
    }
    return $sequence;
  }
}
