<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Directory | Veltrix - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.ico">

    <?php include 'layouts/headerStyle.php'; ?>

    <?php include 'layouts/master.php';
    echo setLayout(); ?>

    <!-- Begin page -->
    <div id="layout-wrapper">
        <?php include 'layouts/topbar.php'; ?>

        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="font-size-18">Directory</h4>
                                <ol class="breadcrumb mb-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Veltrix</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Extra Pages</a></li>
                                    <li class="breadcrumb-item active">Directory</li>
                                </ol>
                            </div>
                        </div>


                        <?php include 'layouts/settingButton.php'; ?>

                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-2.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Jerome A. Hebert</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Jerome@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-3.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Adam V. Acker</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Adam@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>

                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-4.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Stanley M. Dyke</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Stanley@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-5.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Ben J. Mathison</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Ben@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">

                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-6.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">John V. Bailey</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">John@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>

                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-7.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Antonio J. Thomas</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Antonio@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-8.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Jerome A. Hebert</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Jerome@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-9.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font-size-18 mt-0 mb-1">Adam V. Acker</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Adam@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-xl-4 col-md-6">
                            <div class="card directory-card">


                                <div class="card-body">
                                    <div class="media">
                                        <img src="public/images/users/user-10.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg">
                                        <div class="media-body ml-3">
                                            <h5 class="text-primary font--size18 mt-0 mb-1">Stanley M. Dyke</h5>
                                            <p class="font-size-12 mb-2">Creative Director</p>
                                            <p class="mb-0">Stanley@veltrix.com</p>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li><a href="#" class="btn-primary"><i class="mdi mdi-facebook"></i></a>
                                            </li>
                                            <li><a href="#" class="btn-info"><i class="mdi mdi-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <p class="mb-0"><b>Intro : </b>At vero eos et accusamus et iusto odio dignissimos
                                        ducimus qui blanditiis atque corrupti quos dolores et... <a href="#" class="text-primary"> Read More</a></p>
                                </div>




                            </div>
                        </div>
                        <!-- end col -->

                    </div>
                    <!-- end row -->



                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <?php include 'layouts/footer.php'; ?>
        </div>



        <?php include 'layouts/rightbar.php'; ?>
        <?php include 'layouts/footerScript.php'; ?>


        <?php include 'layouts/content-end.php'; ?>