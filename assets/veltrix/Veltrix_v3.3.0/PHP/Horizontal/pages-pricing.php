<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Pricing | Veltrix - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.ico">

    <?php include 'layouts/headerStyle.php'; ?>

    <?php include 'layouts/master.php';
    echo setLayout(); ?>

    <!-- Begin page -->
    <div id="layout-wrapper">
        <?php include 'layouts/topbar.php'; ?>

        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="font-size-18">Pricing</h4>
                                <ol class="breadcrumb mb-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Veltrix</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Extra Pages</a></li>
                                    <li class="breadcrumb-item active">Pricing</li>
                                </ol>
                            </div>
                        </div>


                        <?php include 'layouts/settingButton.php'; ?>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="card pricing-box">
                                <div class="card-body p-4">
                                    <div class="media mt-2">
                                        <i class="ion ion-ios-airplane h2 align-self-center"></i>
                                        <div class="media-body text-right">
                                            <h4 class="mt-0">Starter</h4>
                                            <p class="text-muted mb-0">Sed ut neque unde</p>
                                        </div>
                                    </div>
                                    <div class="pricing-features mt-5 pt-2">
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Free Live Support</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Unlimited User</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> No Time Tracking</p>
                                        <p><i class="mdi mdi-close text-primary mr-2"></i> Free Setup</p>
                                    </div>
                                    <div class="text-center mt-5">
                                        <h1 class="mb-0"><sup><small>$</small></sup>19/<span class="font-size-16">Per
                                                month</span></h1>
                                    </div>
                                    <div class="mt-5">
                                        <a href="#" class="btn btn-primary btn-block waves-effect waves-light">Sign up
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <div class="card pricing-box">
                                <div class="card-body p-4">
                                    <div class="media mt-2">
                                        <i class="ion ion-ios-trophy h2 align-self-center"></i>
                                        <div class="media-body text-right">
                                            <h4 class="mt-0">Professional</h4>
                                            <p class="text-muted mb-0">Sed ut neque unde</p>
                                        </div>
                                    </div>
                                    <div class="pricing-features mt-5 pt-2">
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Free Live Support</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Unlimited User</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> No Time Tracking</p>
                                        <p><i class="mdi mdi-close text-primary mr-2"></i> Free Setup</p>
                                    </div>
                                    <div class="text-center mt-5">
                                        <h1 class="mb-0"><sup><small>$</small></sup>29/<span class="font-size-16">Per
                                                month</span></h1>
                                    </div>
                                    <div class="mt-5">
                                        <a href="#" class="btn btn-primary btn-block waves-effect waves-light">Sign up
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <div class="card pricing-box">
                                <div class="card-body p-4">
                                    <div class="media mt-2">
                                        <i class="ion ion-ios-umbrella h2 align-self-center"></i>
                                        <div class="media-body text-right">
                                            <h4 class="mt-0">Enterprise</h4>
                                            <p class="text-muted mb-0">Sed ut neque unde</p>
                                        </div>
                                    </div>
                                    <div class="pricing-features mt-5 pt-2">
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Free Live Support</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Unlimited User</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> No Time Tracking</p>
                                        <p><i class="mdi mdi-close text-primary mr-2"></i> Free Setup</p>
                                    </div>
                                    <div class="text-center mt-5">
                                        <h1 class="mb-0"><sup><small>$</small></sup>39/<span class="font-size-16">Per
                                                month</span></h1>
                                    </div>
                                    <div class="mt-5">
                                        <a href="#" class="btn btn-primary btn-block waves-effect waves-light">Sign up
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-3 col-md-6">
                            <div class="card pricing-box">
                                <div class="card-body p-4">
                                    <div class="media mt-2">
                                        <i class="ion ion-ios-cube h2 align-self-center"></i>
                                        <div class="media-body text-right">
                                            <h4 class="mt-0">Unlimited</h4>
                                            <p class="text-muted mb-0">Sed ut neque unde</p>
                                        </div>
                                    </div>
                                    <div class="pricing-features mt-5 pt-2">
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Free Live Support</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> Unlimited User</p>
                                        <p><i class="mdi mdi-check text-primary mr-2"></i> No Time Tracking</p>
                                        <p><i class="mdi mdi-close text-primary mr-2"></i> Free Setup</p>
                                    </div>
                                    <div class="text-center mt-5">
                                        <h1 class="mb-0"><sup><small>$</small></sup>49/<span class="font-size-16">Per
                                                month</span></h1>
                                    </div>
                                    <div class="mt-5">
                                        <a href="#" class="btn btn-primary btn-block waves-effect waves-light">Sign up
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->

                    </div>
                    <!-- end row -->
                </div>



            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <?php include 'layouts/footer.php'; ?>
    </div>



    <?php include 'layouts/rightbar.php'; ?>
    <?php include 'layouts/footerScript.php'; ?>



    <?php include 'layouts/content-end.php'; ?>